console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function personalDetails(){
		let firstName = prompt("What is your name?");
		let yourAge = prompt("How old are you?");
		let yourAddress = prompt("Where do you live?");
		alert("Thank you for your input!");
		console.log("Hello, " + firstName);
		console.log("You are " + yourAge);
		console.log("You live in " + yourAddress);
	}

	personalDetails();



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function myFavoriteBands(){
		console.log("My Favorite Bands/Artist are:")
		console.log("1.Imagine Dragons");
		console.log("2.Maroon 5");
		console.log("3.Callalily");
		console.log("4.Taylor Swift");
		console.log("5.Dua Lipa");
	}

	myFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function myFavoriteMovies(){
		console.log("My Favorite Movies are:");

		let firstMovie = "1. Wonder Woman"
		console.log(firstMovie);
		console.log("Rotten Tomatoes Ratings: 93%");


		let secondMovie = "2. Avengers:Endgame"
		console.log(secondMovie);
		console.log("Rotten Tomatoes Ratings: 94%");

		let thirdMovie = "3. Spiderman:No Way Home"
		console.log(thirdMovie);
		console.log("Rotten Tomatoes Ratings: 93%");

		let fourthMovie = "4. Top Gun Maverick"
		console.log(fourthMovie);
		console.log("Rotten Tomatoes Ratings: 96%");

		let fifthMovie = "5. Taken"
		console.log(fifthMovie);
		console.log("Rotten Tomatoes Ratings: 85%");



	}

	myFavoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



// printUsers();

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


// console.log(friend1);
// console.log(friend2);

